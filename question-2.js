/**
 * Question 2
 * Find the total value of multiplication of all the numbers inside array a and array b respectively, 
 * and return the difference.
 * Make sure the output of the function is always positive integer.
 * Exp: findDifference([2,5], [2,3]) will return 4
 * Exp: findDifference([1,4,5], [2,6]) will return 8
 * Exp: findDifference([1,2], [3,4]) will return 10
 * @param array a
 * @param array b
 * @return number
 */
function findDifference(a, b) {
    // write your answer here
}

module.exports = findDifference;