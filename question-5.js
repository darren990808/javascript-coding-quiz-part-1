/**
 * Question 5
 * A Narcissistic Number is a positive number which is the sum of its own digits, 
 * each raised to the power of the number of digits.
 * Return true if the given number is a Narcissistic Number in base 10.
 * Otherwise, return false.
 * Exp: isNarcissisticNumber(153) will return true, as 1^3 + 5^3 + 3^3 = 1 + 125 + 27 = 153
 * Exp: isNarcissisticNumber(1234) will return false, as 1^4 + 2^4 + 3^4 + 4^4 = 1 + 16 + 81 + 256 = 354
 */
function isNarcissisticNumber(value) {
    // write your answer here
}

module.exports = isNarcissisticNumber;
