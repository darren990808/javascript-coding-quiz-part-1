
### Clone the project
```
git clone https://gitlab.com/darren990808/javascript-coding-quiz-part-1.git
```

### Setup the project
```
cd javascript-coding-quiz-part-1
npm install
```

### Open your branch
```
git checkout -b {your short name}
git push -u origin {your short name}
```

### To test whether your solutions pass the all the tests
```
npm run test
```

### Once all the tests are passed, push your solutions to your remote branch
```
git commit -am "quiz done"
git push
```
