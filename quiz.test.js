const removeValuesPresentInBFromA = require('./question-1');
const findDifference = require('./question-2');
const isSameWhenBackward = require('./question-3');
const getHighestAndLowest = require('./question-4');
const isNarcissisticNumber = require('./question-5');

describe("Javascript Quiz Part 2", () => {
    test('Question 1: remove values present in b from a', () => {
        expect(removeValuesPresentInBFromA([1, 2, 3], [2])).toStrictEqual([1,3]);
        expect(removeValuesPresentInBFromA([1, 2, 2, 3, 3, 3, 4], [2, 3])).toStrictEqual([1,4]);
    });

    test('Question 2: find the difference', () => {
        expect(findDifference([2, 5], [2, 3])).toBe(4)
        expect(findDifference([1, 4, 5], [2, 6])).toBe(8)
        expect(findDifference([1, 2], [3, 4])).toBe(10)
    });

    test('Question 3: is palindrome number', () => {
        expect(isSameWhenBackward(1221)).toBe(true);
        expect(isSameWhenBackward(123)).toBe(false);
        expect(isSameWhenBackward(-1221)).toBe(false);
    });

    test('Question 4: get the highest and lowest', () => {
        expect(getHighestAndLowest("1 2 3")).toBe('3 1');
        expect(getHighestAndLowest("-1 4 2 5 -10")).toBe('5 -10');
    });

    test('Question 5: is Narcissistic Number', () => {
        expect(isNarcissisticNumber(153)).toBe(true);
        expect(isNarcissisticNumber(1234)).toBe(false);
    });
});