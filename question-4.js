/**
 * Question 4
 * Given a string of space-seperated numbers, return a string that seperate 
 * the highest and lowest number with a space.
 * Solve this question without using Math.min and Math.max.
 * Exp: getHighestAndLowest("1 2 3") will return '3 1'
 * Exp: getHighestAndLowest("-1 4 2 5 -10") will return '5 -10'
 * @param string numbers
 * @return string
 */
function getHighestAndLowest(numbers) {
    // write your answer here
}

module.exports = getHighestAndLowest;


