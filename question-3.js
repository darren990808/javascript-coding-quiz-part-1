/**
 * Question 3
 * Return true if the function parameter read the same backward as forward. 
 * Otherwise, return false.
 * Write the answer in one line of code.
 * Exp: isSameWhenBackward(1221) will return true
 * Exp: isSameWhenBackward(123) will return false
 * Exp: isSameWhenBackward(-1221) will return false
 * @param number value
 * @return boolean
 */
function isSameWhenBackward(value) {
    // write your answer here
}

module.exports = isSameWhenBackward;