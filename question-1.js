/**
 * Question 1
 * Remove all the values from the array a if the values present in array b.
 * Write the answer in one line of code.
 * Exp: difference([1,2,3], [2]) will return [1,3]
 * Exp: difference([1,2,2,3,3,3,4], [2,3]) will return [1,4]
 * @param array a
 * @param array b
 * @return array
 */
function removeValuesPresentInBFromA(a, b) {
    // write your answer here
}

module.exports = removeValuesPresentInBFromA;